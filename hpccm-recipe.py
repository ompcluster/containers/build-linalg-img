#!/usr/bin/env python

from __future__ import print_function
from functools import partial

import re
import argparse
import hpccm
import os
import itertools
from hpccm.building_blocks import *
from hpccm.primitives import *


def generateRecipePlasma(baseImage):
    Stage0 = hpccm.Stage()

    # Start "Recipe"
    Stage0 += baseimage(image=baseImage)

    if 'centos' in baseImage:
        Stage0 += shell(commands=['curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash'])

    # Install OpenBLAS/Lapack
    Stage0 += comment('OpenBLAS/Lapack installation')
    Stage0 += generic_cmake(cmake_opts=['-DCMAKE_BUILD_TYPE=Release',
                                        '-DBUILD_SHARED_LIBS=ON'],
                            url='https://github.com/xianyi/OpenBLAS/releases/download/v0.3.20/OpenBLAS-0.3.20.tar.gz')

    hpccm.config.set_container_format('docker')

    return Stage0


def generateRecipeSlate(baseImage):
    Stage0 = hpccm.Stage()

    # Start "Recipe"
    Stage0 += baseimage(image=baseImage)

    if 'centos' in baseImage:
        Stage0 += shell(commands=['curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash'])

    # Install OpenBLAS/Lapack
    Stage0 += comment('OpenBLAS/Lapack installation')
    Stage0 += generic_cmake(cmake_opts=['-DCMAKE_BUILD_TYPE=Release',
                                        '-DBUILD_SHARED_LIBS=ON'],
                            url='https://github.com/xianyi/OpenBLAS/releases/download/v0.3.20/OpenBLAS-0.3.20.tar.gz')

    Stage0 += comment('Update cmake')
    Stage0 += cmake(eula=True, version='3.20.0')

    Stage0 += comment('ScaLapack installation')
    Stage0 += generic_cmake(url='http://www.netlib.org/scalapack/scalapack-2.1.0.tgz')

    hpccm.config.set_container_format('docker')

    return Stage0


def combine(list1, list2):
    return map(lambda x: '-'.join(x), list(itertools.product(list1, list2)))


def main():
    print("Generating App image definitions...")

    # list of combinations of configurations that can be used
    allConf = ["ubuntu18.04", "ubuntu20.04", "centos7"]
    allConf += combine(allConf, ["cuda10.1", "cuda10.2", "cuda11.2"])
    allConf = combine(allConf, ["mpich", "mpich-legacy",
                                "openmpi", "openmpi-legacy",
                                "mvapich2"])

    outputFolder = "Dockerfiles"

    for currConf in allConf:
        baseImageDev = "ompcluster/runtime:" + currConf
        baseImageExp = "ompcluster/runtime-dev:" + currConf

        # Generation of Dockerfile for PLASMA
        defFileTextPlasmaDev = str(generateRecipePlasma(baseImageDev))
        defFileTextPlasmaExp = str(generateRecipePlasma(baseImageExp))
        defFileNamePlasmaDev = "plasma-dev-" + currConf
        defFileNamePlasmaExp = "plasma-exp-" + currConf

        # save container definition file
        with open(f"{outputFolder}/{defFileNamePlasmaDev}", "w") as text_file:
            text_file.write(str(defFileTextPlasmaDev))
        with open(f"{outputFolder}/{defFileNamePlasmaExp}", "w") as text_file:
            text_file.write(str(defFileTextPlasmaExp))

        # Generation of Dockerfile for SLATE
        defFileTextSlateDev = str(generateRecipeSlate(baseImageDev))
        defFileTextSlateExp = str(generateRecipeSlate(baseImageExp))
        defFileNameSlateDev = "slate-dev-" + currConf
        defFileNameSlateExp = "slate-exp-" + currConf

        # save container definition file
        with open(f"{outputFolder}/{defFileNameSlateDev}", "w") as text_file:
            text_file.write(str(defFileTextSlateDev))
        with open(f"{outputFolder}/{defFileNameSlateExp}", "w") as text_file:
            text_file.write(str(defFileTextSlateExp))


if __name__ == "__main__":
    main()
